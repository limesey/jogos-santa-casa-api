import { Router } from "express";

import {
  getContestPage,
  scrapeWebpage,
} from "../../../../scrapers/euromilhoes";
import handlePromise from "../../../../util/promiseHandler";

const contestsRouter = Router();

contestsRouter.get("/", async (req, res) => {
  const [resolved, result] = await handlePromise(getContestPage());

  if (resolved == true) {
    res.json(scrapeWebpage(result.data).contests);
  } else {
    console.error(result);
    res.send(500);
  }
});

export default contestsRouter;
