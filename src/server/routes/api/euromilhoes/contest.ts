import { Router } from "express";

import {
  scrapeWebpage,
  getContestPage,
} from "../../../../scrapers/euromilhoes";

import handlePromise from "../../../../util/promiseHandler";

const contestRouter = Router();

contestRouter.get("/", async (req, res) => {
  const contestId = req.query.contestId;

  if (contestId != undefined) {
    let [resolved, result] = await handlePromise(
      getContestPage(contestId.toString())
    );

    if (resolved == false) {
      res.sendStatus(result.status);
      return;
    }

    const data = scrapeWebpage(result.data);

    res.json(data);
  } else {
    res.sendStatus(400);
  }
});

export default contestRouter;
