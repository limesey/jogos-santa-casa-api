import { Router } from "express";

import handlePromise from "../../../../util/promiseHandler";
import {
  scrapeWebpage,
  getContestPage,
} from "../../../../scrapers/euromilhoes";
import contestsRouter from "../euromilhoes/contests";
import contestRouter from "../euromilhoes/contest";

const euromilhoesRouter = Router();

euromilhoesRouter.get("/", async (req, res) => {
  const [resolved, result] = await handlePromise(getContestPage());

  if (resolved == true) {
    res.json(scrapeWebpage(result.data));
  } else {
    console.error(result);
    res.sendStatus(500);
  }
});

euromilhoesRouter.use("/contests", contestsRouter);
euromilhoesRouter.use("/contest", contestRouter);

export default euromilhoesRouter;
