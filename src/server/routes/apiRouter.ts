import { Router } from "express";
import euromilhoesRouter from "./api/euromilhoes/euromilhoes";

const apiRouter = Router();

// TODO: Find a way to dynamilly create routes using files in routes/api
apiRouter.use("/euromilhoes", euromilhoesRouter);

export default apiRouter;
