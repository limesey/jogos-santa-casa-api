import express from "express";

import apiRouter from "./routes/apiRouter";

const app = express();
const PORT = 8080;

// IDEAS:

// Web scrape the data using a scraper
// Store data in DB for ~1h
// When a request is made
//  - Return data from the DB if the data is fresh
//  - Update DB and return new data
//
// OR
// Automatically fetch new data every 1h?

app.use("/api", apiRouter);

app.get("/", (req, res) => {
  res.send("Hello, world!");
});

app.listen(PORT, () => {
  console.log(`Server listening on http://localhost:${PORT}`);
});
