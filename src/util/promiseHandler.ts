/**
 * Util function to allow one liners when dealing with promises. Credit to [fireship.io](https://https://youtu.be/ITogH7lJTyE)
 * @param promise The promise to be resolved
 * @returns
 */
async function handlePromise(promise: Promise<any>) {
  try {
    const result = await promise;

    return [true, result];
  } catch (reason) {
    return [false, reason];
  }
}

export default handlePromise;
