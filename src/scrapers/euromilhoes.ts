import axios from "axios";
import * as cheerio from "cheerio";

import * as fs from "fs";

import handlePromise from "../util/promiseHandler";

const EUROMILHOES_URL =
  "https://www.jogossantacasa.pt/web/SCCartazResult/euroMilhoes";

type Key = {
  numbers: string[];
  stars: string[];
};

type Keys = {
  sortedKey: Key;
  orderedKey: Key;
};

type Prize = {
  placement: string;

  requiredNumberGuesses: string;
  requiredStarGuesses: string;

  winners: {
    portugal: string;
    total: string;
  };
};

type Contest = {
  id: string;
  title: string;
};

type EurmomilhoesData = {
  id: string;
  keys: Keys;
  prizes: Prize[];
  contests: Contest[];
};

function getKey(element: cheerio.Cheerio<cheerio.Element>) {
  const elementText = element.text().trim();
  let splitKey = elementText.split("+");

  const numbers = splitKey[0].split(" ");
  const stars = splitKey[1].split(" ");

  // Not sure why, but an empty string is being inserted into the array
  numbers.forEach((value, i) => {
    if (value == "") {
      numbers.splice(i, 1);
    }
  });

  stars.forEach((value, i) => {
    if (value == "") {
      stars.splice(i, 1);
    }
  });

  return { numbers: numbers, stars: stars };
}

/**
 * Scrape contests data from the selection menu
 */
function getContests($: cheerio.CheerioAPI) {
  const selectionMenuOptions = $("select[name=selectContest] option");
  const contests: Contest[] = [];

  selectionMenuOptions.each(function (elem, i) {
    const id = $(this).attr("value")!;
    const title = $(this).text();

    contests.push({ id: id, title: title });
  });

  return contests;
}

/** */
function storeData() {}

function getContestPage(contestId?: string) {
  return new Promise(async function (resolve, reject) {
    if (contestId != undefined) {
      const [resolved, result] = await handlePromise(
        axios.post(EUROMILHOES_URL + `?selectContest=${contestId}`)
      );

      if (resolved == true) {
        resolve(result);
      } else {
        console.error(result);
        reject(result);
      }
    } else {
      const [resolved, result] = await handlePromise(
        axios.get(EUROMILHOES_URL)
      );

      if (resolved == true) {
        resolve(result);
      } else {
        reject(result);
      }
    }
  });
}

/** */
function scrapeWebpage(content: string | Buffer): EurmomilhoesData {
  // fs.writeFileSync("index.html", result.data); */

  //const result = fs.readFileSync("index.html");

  let data: EurmomilhoesData = {
    id: "",
    keys: {
      orderedKey: {
        numbers: [],
        stars: [],
      },
      sortedKey: {
        numbers: [],
        stars: [],
      },
    },

    prizes: [],

    contests: [],
  };

  const $ = cheerio.load(content);

  getContests($);

  const keyLists = $(".betMiddle.twocol.regPad ul li");

  if (keyLists.length == 0) {
    return data;
  }

  const selectedContest = $("select[name=selectContest] option");
  const contestId = selectedContest.first().attr("value");

  const orderedKey = getKey(keyLists.first());
  const sortedKey = getKey(keyLists.eq(1));

  const prizesDiv = $(".stripped.betMiddle.customfiveCol.regPad ul");

  const prizes: Prize[] = [];

  prizesDiv.each(function (i, elem) {
    const column = $(this).find("li");

    const rightGuessesRow = column.eq(1).text();
    // TODO: Learn Regex and use it here
    const rightGuessesSplit = rightGuessesRow.split("+");

    const rightnumbers = rightGuessesSplit[0].split(" ")[0];
    const rightStars = rightGuessesSplit[1].split(" ")[1];

    const portugalWinners = column.eq(2).text();
    const totalWinners = column.eq(3).text();

    const prize = {
      placement: (i + 1).toString(),
      requiredNumberGuesses: rightnumbers,
      requiredStarGuesses: rightStars,

      winners: {
        portugal: portugalWinners,
        total: totalWinners,
      },
    };

    prizes.push(prize);
  });

  data.keys = {
    orderedKey: orderedKey,
    sortedKey: sortedKey,
  };

  data.prizes = prizes;

  data.contests = getContests($);

  data = {
    id: contestId!,
    keys: {
      orderedKey: orderedKey,
      sortedKey: sortedKey,
    },

    prizes: prizes,
    contests: getContests($),
  };

  return data;
}

export { getContestPage, scrapeWebpage };
