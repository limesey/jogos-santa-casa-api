# jogos-santa-casa-api

A REST API to check the keys for the latest draws of Jogos Santacasa.

## Project scope

This project was created with the scope of learning how to web scrape using JavaScript/TypeScript, by using the Cheerios library. Used to also practice creating a REST API.

The project can be expanded upon in order to cover the other lottery tickets, however, as this was a practice project, I do not see how repeating the same process for the others would benefit me or my learning path. That said, feel free to fork it, use and expand it to your needs.

## Dependencies

- [Node.js](https://nodejs.org/) Used to run JS
- Packages listed in package.json
  - Axios: HTTP requests library
  - Cheerios: Used for web scraping
  - Express: Used to create the web server
